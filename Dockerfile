FROM node:17 
# Create app directory  
WORKDIR /home/jamoulqu/calculator
COPY package*.json ./  
# Install app dependencies  
RUN npm install  
COPY . .  
EXPOSE 3522  
CMD [ "node", "server.js" ]  