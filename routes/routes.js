const express = require('express')
const calculator = require('../controllers/calculator-controller')
const router = express.Router()

router.post('/calculate', calculator.calculate)

module.exports = router