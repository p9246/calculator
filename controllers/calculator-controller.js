exports.calculate = function (req, res) {

    let i;
    let result = 0;

    let validoperators = ['*', '/', '+', '-'];
    let priorities = [
        ['*', '/'],
        ['+', '-']
    ];

    let tmpOperand = '';

    let operands = [];
    let operators = [];


    // Separation of operators and operands
    for(i=0; i < req.body.calcul.length; i++) {
        if((req.body.calcul.charAt(i) >= '0' && req.body.calcul.charAt(i) <= '9') || req.body.calcul.charAt(i) == '.') {
            tmpOperand += req.body.calcul.charAt(i);
        }
        else {
            operators.push(req.body.calcul.charAt(i));
            operands.push(+(tmpOperand));
            tmpOperand = '';
        }
    }

    // If the last character is an operator, we resend the calcul
    if(!tmpOperand.length) {
        result = req.body.calcul;
    }
    else {
        operands.push(+(tmpOperand));
    }

    i = 0;
    // Verification of operators (valid or not)
    operators.forEach(operator => {
        if(validoperators.includes(operator) == false) {
            res.status(422).json({ 'error': 'No valid operator encoded.' })
            return;
        }
        else if(operator == '/' && operands[i+1] == 0) {
            result = 'Infinity';
        }

        i++;
    });

    // Calculate with priorities
    if(result == 0)
    {
        priorities.forEach(ops => {
            i = 0;
            while(i < operators.length) {
                ops.forEach(op => {
                    if(i >= 0 && operators[i] == op)
                    {
                        switch(operators[i])
                        {
                            case '*': operands[i] = mult(operands[i], operands[i+1]);
                                break;
                            case '/': operands[i] = div(operands[i], operands[i+1]);
                                break;
                            case '+': operands[i] = add(operands[i], operands[i+1]);
                                break;
                            case '-': operands[i] = sub(operands[i], operands[i+1]);
                                break;
                        }
                        
                        // Remove used operator and operand from the calcul
                        operators.splice(i, 1);
                        operands.splice(i+1, 1);
    
                        i--;
                    }
                });
    
                i++;
            };
        });
    
        result = operands[0];
    }

    return res.status(200).json(result);

    function add(a, b) {
        return parseFloat(a) + parseFloat(b);
    }
    
    function sub(a, b) {
        return parseFloat(a) - parseFloat(b);
    }
    
    function div(a, b) {
        return parseFloat(a) / parseFloat(b);
    }
    
    function mult(a, b) {
        return parseFloat(a) * parseFloat(b);
    }

}