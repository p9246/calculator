const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const routes = require('./routes/routes');
const PORT = process.env.PORT || 3522;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let corsOptions = {
  credentials: true,
  origin: 'http://localhost:4200',
  optionsSuccessStatus: 200
}

app.use(cors(corsOptions))
app.use('/routes', routes);

app.get('/', (req, res) => {
  res.send('Serveur Calculator v1')
})

app.listen(PORT, () => {
  console.log(`Server is running on PORT ${PORT}`);
});